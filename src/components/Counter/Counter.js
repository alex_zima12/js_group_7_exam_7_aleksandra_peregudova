import React from 'react';
import './Counter.css'

const Counter = props => {
    let price = [];
    let sum = 0;
    props.menu.forEach((dish) => {
        price.push(dish.count * dish.price);
    });
    for (let i = 0; i < price.length; i++) {
        sum = sum + parseInt(price[i]);
    }
    return (
        <div className="counter">
            Total price: {sum}
        </div>
    );
};

export default Counter;