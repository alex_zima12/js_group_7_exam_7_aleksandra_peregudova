import React from 'react';
import './OrderItem.css'

const OrderItem = (props) => {
    return (
        <div className="orderItem">
            <p className="dish">{props.name}</p>
            <p> {props.count} * {props.price}</p>
            <button className="btn" onClick={() => props.deleteItemFromOrder(props.keyIndex)}>Delete</button>
        </div>
    );
};

export default OrderItem;

