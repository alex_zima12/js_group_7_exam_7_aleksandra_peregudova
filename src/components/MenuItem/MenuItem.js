import React from 'react';
import './MenuItem.css'

const MenuItem = (props) => {
    return (
        <div className="MenuItem" onClick={()=>props.addItemToOrder(props.keyIndex)}>
            <img className="MenuPic" src={props.image} alt="dishImg"/>
            <p>{props.name}</p>
            <p>Price: {props.price}</p>
        </div>
    );
};

export default MenuItem;