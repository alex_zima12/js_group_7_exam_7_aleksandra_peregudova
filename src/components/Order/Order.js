import React from 'react';
import OrderItem from "../OrderItem/OrderItem";

const Order = (props) => {
    const orderList = props => {
        const arr = [];
        props.menu.forEach((item, index) => {
                if (item.count > 0) {
                    arr.push(<OrderItem
                        key={"orderItem" + index}
                        keyIndex={index}
                        name={item.name}
                        price={item.price}
                        count={item.count}
                        deleteItemFromOrder={props.deleteItemFromOrder}
                    />)
                }
            }
        );
        return arr;
    };

    return (
        <div className="totalOrder">
            {orderList(props)}
        </div>
    );
};

export default Order;