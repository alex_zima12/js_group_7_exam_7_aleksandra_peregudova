import React, {useState} from 'react';
import './App.css';
import HamburgerImage from './assets/Burg.jpeg';
import CheeseburgerImage from './assets/ChBurg.png';
import FriesImage from './assets/Fries.jpeg';
import CoffeeImage from './assets/Coffee.jpeg';
import TeaImage from './assets/Tea.jpeg';
import ColaImage from './assets/Cola.jpeg';
import MenuItem from "./components/MenuItem/MenuItem";
import Order from "./components/Order/Order";
import Counter from "./components/Counter/Counter";

const App = () => {
    const [menu, setMenu] = useState([
        {name: 'Hamburger', count: 0, price: 80, image: HamburgerImage},
        {name: 'Cheeseburger', count: 0, price: 90, image: CheeseburgerImage},
        {name: 'Fries', count: 0, price: 45, image: FriesImage},
        {name: 'Coffee', count: 0, price: 70, image: CoffeeImage},
        {name: 'Tea', count: 0, price: 50, image: TeaImage},
        {name: 'Cola', count: 0, price: 40, image: ColaImage},
    ]);

    const menuItems = menu.map((item, index) => {
        return <MenuItem
            key={"menuItem" + index}
            keyIndex={index}
            image={menu[index].image}
            name={menu[index].name}
            price={menu[index].price}
            addItemToOrder={(indexItem) => {
                addItemToOrder(indexItem);
            }}
        />
    });

    const addItemToOrder = (index) => {
        const copyMenu = [...menu];
        let itemCount = {...copyMenu[index]};
        itemCount.count++;
        copyMenu[index] = itemCount;
        setMenu(copyMenu);
    };

    const deleteItemFromOrder = (index) => {
        const copyMenu = [...menu];
        let itemCount = {...copyMenu[index]};
        itemCount.count = 0;
        copyMenu[index] = itemCount;
        setMenu(copyMenu);
    };

    return (
        <div className="App">
            <div className="container">
                <div className="order">
                    <h1>Here You can make an order!</h1>
                    {menuItems}
                </div>
                <div className="menuList">
                    <h1>Order Details</h1>
                    <Order
                        menu={menu}
                        deleteItemFromOrder={(index) => {deleteItemFromOrder(index);}}
                    />
                    <div>
                        <Counter
                            menu={menu}
                        />
                    </div>
                </div>
            </div>

        </div>


    );
}

export default App;
